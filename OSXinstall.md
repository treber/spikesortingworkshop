# Workshop on Spike-Sorting

This repository entails code and infos for the one-day workshop on spike-sorting using [wave_clus](https://github.com/csn-le/wave_clus) [combinato](https://github.com/jniediek/combinato)


# Installation of Software

## Install git

MacOs

``` 
brew install git
```

## Install Combinato using macOs Mojave
### Install Python 2.7 and required packages
The easisest way to get Python this is using Anconda - it should already come with all required pacakges.

To install python 2.7 with anaconda, follow the instructions given here: [https://conda.io/docs/user-guide/install/](https://conda.io/docs/user-guide/install/)

Then, create a python 2.7 environment...

```bash
conda create -n python2 python=2.7 anaconda
```

... and activate it: 

```
source activate python2
```

This will put that environment (typically ~/anaconda/envs/python2) in front in your PATH, so that when you type python at the terminal it will load the Python from that environment. 

I had to downgrade from pyqt version 5 to 4 by the following: 

```shell
conda install pyqt=4
```

Make a copy of the options file:

``` shell
cp combinato/default_options.py to combinato/options.py
```

and make sure that the path to spc binary in `options.py` fits to the OS you are using. In my case, it says:

```
CLUS_BINARY = '/Users/treber/scripts/combinato/spc/cluster_mac_new.exe'
```

Add combinato scripts to your shells path either by editing `/etc/paths`,  `~/.profile`, `.bashrc`, or something similar

```
PATH=$PATH:/Users/treber/scripts/combinato
```

Set Python path by adding these two lines to `.profile` or the like

```
PYTHONPATH=$PYTHONPATH:/Users/treber/scripts/combinato
export PATH PYTHONPATH
```

To test the installation:

```
cd [combinatodir]/tools
python test_installation.py
```
The output should look something like this:

```
Found Combinato
Found SPC binary
Your version of pytables is 3.4.4
Combinato clustering setup: no problems detected.
Found 'montage', plotting continuous data possible.
```

## Install wave_clus
Here, we assume, that you already have installed matlab.

Note that wave_clus has recently received an update to version 3. To install simply clone the repository from git:

```
git clone https://github.com/csn-le/wave_clus.git
```

... and set matlab paths accordingly, e.g. by adding these lines

```Matlab
addpath(genpath('/path/to/wave_clus_3'));
```
to your `startup.m` 



