# Materials for our Spike Sorting Worksop
### Example Data:

* [Data](https://1drv.ms/f/s!Ar6O6_m2VBtAiN1lVNLUW0GMv2eVaw)
* The password is "LetsDoSomeSpikeSortingNow"

### Resources for Combinato 
* [Anaconda ](https://www.anaconda.com/download/)(recommended software to handle multiple python versions and packages on the same machine)
* [Original Combinato to be used with Linux or MacOS](https://github.com/jniediek/combinato)
* [Marijes Fork for Windows](https://github.com/marijeterwal/combinato)


### Resources for wave_clus
* [MATLAB](wwww.mathworks.com)
* [wave_clus](https://github.com/csn-le/wave_clus)

### Some additional scripts 
These links point to repositories that come with the usual "use at your own risk" disclaimer.

* [scripts shared between people in Bonn]()
* [some additional scripts we use in the workshop](https://bitbucket.org/treber/spikesortingworkshop/src/master/)
