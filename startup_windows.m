addpath(genpath('C:\common_analysis_scripts\'));
% windows has issues with linux-style soft links, so remove these from path
rmpath('C:\common_analysis_scripts\remove_spikes_standalone');
rmpath('C:\common_analysis_scripts\references_standalone');
addpath(genpath('C:\wave_clus'));
addpath('C:\spikesortingworkshop');