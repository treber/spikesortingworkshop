function extract_events_ospr;

%% do this one stepwise in the matlab-prompt form excess = ... to
%% excess =...
keyboard

if strcmp(computer,'PCWIN')
    [Events_TimeStamps, EventIDs, Events_Nttls, Extras, EventStrings] = nlx_EVRead('Events.Nev');
    clear EventIDs; clear Extras; clear EventStrings;
else
    [Events_TimeStamps, Events_Nttls] = nevRead('Events.nev');
end


Nreps=10;    %max. anzahl durchlaeufe

create_stimulus_struct;
load stimulus;
Npics=length(stimulus);

excess=length(Events_TimeStamps)-((Npics+1)*4*Nreps+Nreps)*2-8
%ersma bis hierhin, excess gibt an wie viele ueberschuessige Events
%aufgezeichnet wurden

%krumme Zahlen beim reset rauswerfen (z.B. von 7 auf 0):
A=find(Events_Nttls);
c=find(diff(A)~=2);
if length(c)>0,
    warning('invalid timestamps')
    [val ind]=max([Events_Nttls(A(c));Events_Nttls(A(c)+1)]);
    Events_Nttls(A(c))=val;
    Events_TimeStamps(A(c))=Events_TimeStamps(A(c)+ind-1);
    Events_Nttls(A(c)+1)=[];
    Events_TimeStamps(A(c)+1)=[];
end

%ueberschuessige Nullen am Ende?
if sum(Events_Nttls(end-1:end))==0,
    Events_Nttls(end)=[];
    Events_TimeStamps(end)=[];
end

%ueberschuessige Nullen am Anfang?
if Events_Nttls(1)==0,
    Events_Nttls(1)=[];
    Events_TimeStamps(1)=[];
end

% JN 2012/11/21
% for adaptive:
% total = 190
% second = 90
% third = 10
% excess = length(Events_TimeStamps) - ((total*4*3+3 + second*4*3+3 + third*4*3+3)*2 + 8)

% Falls irgendwo Doppelnullen stehen (bei 015 passiert), folgenden code ausfuehren:
Events_TimeStamps(find(diff(Events_Nttls)==0))=[];
Events_Nttls(find(diff(Events_Nttls)==0))=[];


excess=length(Events_TimeStamps)-((Npics+1)*4*Nreps+Nreps)*2
%%% 1. BIS HIERHIN
%wenn excess jetzt 0, wurden erfolgreich alle Ueberfluessigen eliminiert,
%wenn nicht, Fehlersuche!

%Ueberpruefen, ob die timestamps zu beginn eines jeden durchgangs richtig
%stehen, und dann rauswerfen:
%should all be zeros
diff([find(Events_Nttls==11) length(Events_Nttls)+1]) %-Npics*8-2

Events_TimeStamps(find(Events_Nttls==11)+1)=[];
Events_Nttls(find(Events_Nttls==11)+1)=[];
Events_TimeStamps(find(Events_Nttls==11))=[];
Events_Nttls(find(Events_Nttls==11))=[];

Events_TimeStamps(1:find(Events_Nttls==1,1)-1)=[];
Events_Nttls(1:find(Events_Nttls==1,1)-1)=[];


%jetzt ist alles gut und wir weisen das ergebnis zu:
Event_Time=Events_TimeStamps(1:2:end);
Event_Value = Events_Nttls(1:2:end);

% tt_Event_Value=diff(Event_Value);
% exclude=find(mod(tt_Event_Value,2)==0)+1
% excess=excess-length(exclude)
% Event_Time(exclude)=[];
% Event_Value(exclude)=[]; 

%jetzt noch alle verschieben, damit kompatibel zum normalen screening
%(erstmals bei 010 aufgefallen)
%Event_Time(1:end-1)=Event_Time(2:end);
%Event_Value(1:end-1)=Event_Value(2:end);


if length(Event_Time)+1 -(Npics+1)*4*Nreps~=0, error('invalid number of events'); end

tt=diff(Event_Time)/1000;

titles(1).title = 'Presentation/Reaction Time';
titles(2).title = 'Flip Time';
titles(3).title = 'Jitter Time (and breaks btw. runs)';
titles(4).title = 'Fixation Cross Time';

for i=1:4
    subplot(2,2,i);
    plot(tt(i:4:end));
    title(titles(i).title);
    ylabel('ms');
    xlabel('Pres. No.');
    if i==3, ylim([0 600]); end
end
%should be 1000 ms = anzeigedauer bilder
%plot(tt(2:4:end)); %should be variable (response)
%plot(tt(3:4:end)); %should be 70 ms for Windows, 2ms for Linux
%plot(tt(4:4:end)); %should be 500+140+-50 ms (or around 140 for fastscr under Windows, 180 for fastscr under linux)

%%% 2. BIS HIERHIN
% wenn plots ok 

save finalevents Event_Time Event_Value; %-v6;
%% 3. BIS HIERHIN

%so, fertig! jetzt nur noch sites speichern (ganz unten, letzte 3 zeilen)

sites=['RA  ';'REC ';'RAH ';'RPHG';'LA  ';'LEC ';'LAH ';'LPHG']
sites=['REC ';'RAH ';'RPHG';'LTO ';'RTO ';'ROF ';'LAH ';'LPHG']
sites=['REC ';'RAH ';'RPHG';'ROF ';'RTO ';'LAH ';'LPHG';'LTO ']
sites=['RA  ';'REC ';'RAH ';'RPHG';'LA  ';'LAH ';'LPHG';'LTO ']
sites=['RA  ';'RPHG';'RPAR';'LSMA';'RAC ';'LAH ';'LPHG';'LPAR']

sites=['RA  ';'RPHG';'RAC ';'LA  ';'LAH ';'LPHG';'LSMA';'LPAR']
sites=['RA  ';'RPHG';'RPAR';'ROF ';'LA  ';'LAH ';'LPHG';'LPAR']

sites=['RA  ';'REC ';'RAH ';'RPHG';'LA  ';'LEC ';'LAH ';'LPHG']

sites=['RA  ';'REC ';'RAH ';'RPHG';'LEC ';'LAH ';'LPHG';'LA  ']

sites=['RA  ';'REC ';'RAH ';'RPHG';'ROF ';'LEC ';'LAH ';'LPHG']

sites=['RA  ';'REC ';'RAH ';'RPHG';'LA  ';'LEC ';'LAH ';'LPHG']
sites=['RA  ';'REC ';'RAH ';'ROF ';'LA  ';'LEC ';'LAH ';'LOF ']
sites=['RA  ';'RAH ';'RPGa';'RPGp';'LA  ';'LEC ';'LAH ';'LPGa']

sites=['RA  ';'RAH ';'RPGp';'LA  ';'LEC ';'LAH ';'LPGa';'LPGp']

sites=['RAH ';'RRAC';'RPC ';'LAH ';'LPHG';'LRAC';'LDAC';'LPC ']

sites=['RA  ';'RAH ';'RPHG';'RST ';'LA  ';'LAH ';'LPHG';'LST ']
sites=['LPHG';'LAH ';'LEC ';'LA  ';'RPHG';'RAH ';'REC ';'RA  ']
sites=['LPHG';'LAH ';'LEC ';'LA  ';'RPHG';'RAH ';'REC ';'RA  ']
sites=['RAH ';'RPHG';'RTO ';'RO  ';'LAH ';'LPHG';'LTO ';'LO  ']
sites=['RA  ';'RPHG';'LA  ';'LAH ';'LIAS';'LIPS';'----';'----']

sites=['REC ';'ROF ';'RAC ';'RSMA';'LA  ';'LEC ';'LAH ';'LAC '] %374
sites=['RA  ';'RMH ';'LA  ';'LEC ';'LMH ';'LOF ';'LAC ';'----'] %375
sites=['RA  ';'REC ';'RAH ';'RPG ';'ROF ';'LA  ';'LEC ';'LMH '] %377
sites=['ROF ';'RSMA';'RP  ';'LOF ';'LAC ';'LpSM';'LSMA';'LP  '] %382
sites=['RA  ';'REC ';'RMH ';'RPHG';'LA  ';'LEC ';'LMH ';'LPHG'] %414

sites=['RA  ';'REC ';'RAH ';'RPHC';'LA  ';'LEC ';'LAH ';'LPHC'] %001

sites=['LA  ';'LAH ';'LEC ';'LPHC';'RA  ';'RAH ';'REC ';'RPHC'] %002
sites=['LAH ';'LEC ';'LMH ';'LPH ';'LPHC';'RA  ';'RAH ';'RPHC'] %003

sites=['LA  ';'LAH ';'LMH ';'LPH ';'RA  ';'RAH ';'REC ';'RPHC'] %003 Do, Fr, 

sites=['LA  ';'LAH ';'LEC ';'LPHC';'RA  ';'RAH ';'REC ';'RPHC'] %004

sites=['LA  ';'LAH ';'LMH ';'LPHC';'RA  ';'RAH ';'RMH ';'RPHC'] %007
sites=['LA  ';'LAH ';'LMH ';'LEC ';'RA  ';'RAH ';'RMH ';'REC '] %008 JN %%ist falsch, muessen alphabetisch sein

sites=['LA  ';'LAH ';'LEC ';'LMH ';'LPHC';'RA  ';'RAH ';'REC ';'RMH ';'RPHC'] %009
sites=['LA  ';'LAH ';'LEC ';'LI  ';'LMH ';'LPH ';'LPHC';'RA  ';'RAH ';'REC ';'RPH ';'RPHC'] %010

sites=['LA  ';'LAH ';'LEC ';'LPHC';'RA  ';'RAH ';'REC ';'RPHC'] %011

sites=['LA  ';'LAH ';'LEC ';'LMH ';'LPHC';'RA  ';'RAH ';'REC ';'RMH ';'RPHC'] %012
sites=['LA  ';'LAH ';'LEC ';'LMH ';'LPHC'; 'LTP '; 'RA  ';'RAH ';'REC ';'RPHC'] %013

sites=['LA  ';'LAH ';'LEC ';'LPHC';'RA  ';'RAH ';'REC ';'RPHC'] %014
sites=['LA  ';'LAH ';'LEC ';'LPHC';'RA  ';'RAH ';'REC ';'RPHC'] %015
sites=['LA  ';'LAH ';'LEC ';'LPHC';'RA  ';'RAH ';'REC ';'RPHC'] %016
sites=['LA  ';'LAH ';'LEC '; 'LPH ' ;'LPHC' ;'RA  ';'RAH ';'REC '; 'RPH ';'RPHC'] %017
sites=['LA  ';'LAH ';'LEC '; 'LMH ' ;'LPH ' ;'LPHC'] %018

sites=['LA  ';'LAH ';'LEC '; 'LMH ' ;'LPHC' ;'RA  ';'RAH ';'REC '; 'RMH ';'RPHC'] %020
sites=['AHL ';'AHR ';'AL  '; 'AR  ' ;'ECL ' ;'ECR ';'MHL ';'MHL '; 'PHCL';'PHCR'] %020 klinisch

sites=['LA  ';'LAH ';'LEC '; 'LMH ' ;'LPH '; 'LPHC' ;'RA  ';'RAH ';'REC '; 'RMH '; 'RPH '; 'RPHC'] %023

sites=['LA  ';'LAH ';'LEC '; 'LMH ' ;'LPH '; 'LPHC' ;'RA  ';'RAH ';'REC '; 'RMH '; 'RPH '; 'RPHC'] %024


sites=['LA  ';'LAH '; 'LEC '; 'LMH ' ; 'LPHC' ;'RA  ';'RAH ';'REC '; 'RMH '; 'RPHC'] %030

sites=['LA  ';'LAH '; 'LEC '; 'LMH ' ; 'LPHC' ;'RA  ';'RAH ';'REC '; 'RMH '; 'RPHC'] %031

sites=['LA  ';'LAH ';'RA  ';'RAH '] %032

sites=['LA  ';'LAH '; 'LEC '; 'LMH ' ; 'LPHC' ;'RA  ';'RAH ';'REC '; 'RMH '; 'RPHC']% 033
sites=['LA  ';'LAH ';'LEC '; 'LMH ' ;'LPH '; 'LPHC' ;'RA  ';'RAH ';'REC '; 'RMH '; 'RPH '; 'RPHC'] %034

sites=['LA  ';'LAH ';'LEC '; 'LMH ' ;'LPH '; 'LPHC' ;'RA  ';'RAH ';'REC '; 'RMH '; 'RPH '; 'RPHC'] %035

sites=['LA  ';'LAH ';'LEC '; 'LMH '; 'LPHC' ;'RA  ';'RAH ';'REC '; 'RMH '; 'RPHC'] %044

%% 4. BIS HIERHIN falls sites = unklar, type(ChannelNames.txt)
ch_per_macro=8;
save sites sites ch_per_macro

