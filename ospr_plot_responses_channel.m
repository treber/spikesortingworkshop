function ospr_plot_responses_channel(channel)

%channel = 8;
%session = '052e04ospr1';

disp(sprintf('loading channel %d', channel));
%% laoding data & infos

% get original channelname
channelnames=textread('ChannelNames.txt', '%s');
channelname = channelnames{channel};
sessionname = pwd;

% segemtnation
segmentstart = -1000; %ms relative to TTL
segmentstop = 2000; %ms reltive to TTL 

% for calculating response criterion
bsl_period = [-500 0]; % ms reltive to TTL
response_period = [100 1000]; 

% load results from spike-sorting
spikes = load(sprintf('times_CSC%d.mat', channel));
clusters = unique(spikes.cluster_class(:,1));
nclusters = sum(clusters>0); % cluster 0 consists of unassigned spikes

% load infos about the design of the experiment
load TrialInfos; % loads trials struct from ospr
stimuli = unique(trials.imagename);
nstimuli = numel(stimuli);
ntrials = numel(trials.imagename);

% load TTL codes and timestamps
events = load('finalevents.mat');
onsettimes = events.Event_Time(events.Event_Value == 1); 
onsettimes = onsettimes/1000; % convert from mikro- to milliseconds
                                                                
assert(numel(onsettimes) == 1010); % there were 1010 trials in this experiment

disp(sprintf('segmenting spikes', channel));
%% segemt spike-times
for ci = 1:nclusters
    
    idxtospikes = spikes.cluster_class(:,1)==ci;
    spikes_in_cluster = spikes.cluster_class(idxtospikes,2);
    
    for ti = 1:ntrials
        % get indices to spikes in segment of interest
        spikeidx = onsettimes(ti)+segmentstart < spikes_in_cluster & ...
            onsettimes(ti)+segmentstop  > spikes_in_cluster;        
        
        % save times relative to TTL in new data structure
        cherries(ci).trial{ti} = spikes_in_cluster(spikeidx) - ...
            onsettimes(ti);        
        
    end
end

disp(sprintf('calculating response criterion', channel));
%% calcuate a response criterion for each stimulus
pvals = NaN(nclusters, nstimuli);
consider = false(nclusters, nstimuli);

for si = 1:nstimuli
    
    stimidx = strcmp(stimuli{si}, trials.imagename);
    for ci = 1:nclusters
        
        [pvals(ci, si) consider(ci, si)] = ...
            binwise_ranksum(cherries(ci).trial(stimidx), ...
                        cherries(ci).trial, ...
                        bsl_period, response_period);
        
    end
    
end

disp(sprintf('plotting best responses', channel));
%% Plot best responses

% params for kernel convolution
kernelwidth = 0.05; %seconds
resolution = 0.001; % seconds 

% params for pltting in general and figure setup
howmany = 8; % stimuli
nrows = nclusters;
ncols = howmany + 2;
fontSize = 8;

sbpos = setup_plot(nrows, ncols);

figh = figure('color', 'w', 'visible', 'off');
set(figh, 'PaperUnits', 'centimeters');
set(figh, 'PaperSize', [30, 3*nclusters]);
set(figh, 'PaperPosition', [0 0 30, 3.2*nclusters]);
%print(figh, '-dpng', '-r300', sprintf('ospr_responses_C%d_%s.png', channel, channelname));


for ci = 1:nclusters

    % display the unit density plot
    pos1 = squeeze(sbpos(ci, 1, :));
    pos1(1) = pos1(1) + 0.3 * pos1(3);
    pos1(3) = pos1(3)*1.6;
    pos1(4) = pos1(4)*0.7;
    subplot('Position', pos1);
    spkidx = spikes.cluster_class == ci;
    density_plot(spikes.spikes, spkidx);
    title(sprintf('%s Class %d', channelname, ci));

    
    % sort stimuli according to the response criterion
    [pvals_sorted, stimidx] = sort(pvals(ci,:), 'ascend');
    
    clear ifraxes; % handels to ifr plots
    
    for si = 1:howmany
        
        % setup subplot for rsater and ifrs
        pos1 = squeeze(sbpos(ci, si+2, :));
        rasterpos = [pos1(1); (pos1(2) + pos1(4) * 0.3); pos1(3); pos1(4)*0.7];
        ifrpos = pos1;
        ifrpos(4) = ifrpos(4)*0.3;
        ax1 = axes('Position', rasterpos);
        
        % get trial indices
        tidx = strcmp(stimuli{stimidx(si)}, ...
                      trials.imagename);
        
        % do a raster plot
        spikes_toplot = cherries(ci).trial(tidx);
        plot_raster(spikes_toplot, [-500:1500]);
        plot([1000 1000], [0 1], ':k');
        
        
        title(stimuli{stimidx(si)}, 'interpreter', 'none', 'FontWeight', ...
              'normal')
        
        % plot instantaneous firing rates
        ax2 = axes('Position', ifrpos);
        ifrs = convolve_spikes(spikes_toplot,-1:resolution:2,kernelwidth, ...
                               resolution); 

        [hdl f ] = plot_signals(ifrs(:,500:2500), [-500:1500], ...
                                'k');
        xlim([-500 1500]);
        plot([1000 1000], ylim, ':k');
        
        ifraxes(si) = gca;
        
        if si == 1 & ci == nclusters
            xlabel('ms');
            ylabel('fr(Hz)');
        else
            set(gca, 'XTick', [])
            xlabel('');
            ylabel('');
        end
        
    end
    linkaxes(ifraxes); % do the same limiits on all ifr plots
end

set(findall(gcf,'type','text'),'FontSize',fontSize,'fontWeight','normal');
set(findall(0,'type','axes'),'FontSize',fontSize,'fontWeight','normal');
print(figh, '-dpng', '-r300', sprintf('ospr_responses_C%d_%s.png', channel, channelname));

disp('-----------------') 
disp(' ' );