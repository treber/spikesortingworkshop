Materials for our Spike Sorting Workshop
========================================

### Literature on Spike-Sorting
-   [Non-technical Introduction (Quian Quiroga, 2012)](https://www2.le.ac.uk/departments/engineering/research/bioengineering/neuroengineering-lab/Publications/spike%20sorting%20quick%20guide.pdf)
-   [Detailed Review (Rey, Pedreira, Quian Quiroga, 2015)](https://www2.le.ac.uk/centres/csn/publications-1/Publications/past-present-and-future-of-spike-sorting-techniques)

### Example Data:

-   [Data](https://1drv.ms/f/s!Ar6O6_m2VBtAiN1lVNLUW0GMv2eVaw)
-   The password is "LetsDoSomeSpikeSortingNow"

### Resources for Combinato

-   [Niediek et al., 2016 (paper that introduces combinato)](https://journals.plos.org/plosone/article/file?id=10.1371/journal.pone.0166598&type=printable)
-   [Anaconda](https://www.anaconda.com/download/) (recommended software to handle multiple python versions and packages on the same machine)
-   [Original Combinato to be used with Linux or MacOS](https://github.com/jniediek/combinato)
-   [Marijes Fork for Windows](https://github.com/marijeterwal/combinato)

### Resources for wave\_clus

-   [Chaure, Rey & Quian Quiroga, 2018 (paper on latest wave_clus version) ](https://www2.le.ac.uk/centres/csn/publications-1/2018/a-novel-and-fully-automatic-spike-sorting-implementation-with-variable-number-of-features-2018)
-   [MATLAB](https://www.mathworks.com)
-   [wave\_clus](https://github.com/csn-le/wave_clus)


### Some additional scripts

These links point to repositories that come with the usual "use at your own risk" disclaimer.

-   [scripts shared between people in Bonn](https://bitbucket.org/treber/common_analysis_scripts.git)
-   [some additional scripts (actually this repository here)](https://bitbucket.org/treber/spikesortingworkshop.git)


