tic;
move_macros; 
ren_batch; 
Get_spikes('all', 'parallel', true);
Do_clustering('all', 'parallel', true);
fprintf('Automated clustering took %.2f Min.\n', toc/60);
fprintf('Done \n****\n')
