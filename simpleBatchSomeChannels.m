tic;
move_macros; 
ren_batch;

% do the following only on these channels
channels = 5;

% change some params for detection
params.stdmin = 5; % 5 default the lowest acceptable value for detection threshold (in units of SD, I guess) higher values: less spikes                                                                                 
Get_spikes(channels, 'parallel', false, 'par', params);

% change some params for sorting
params.template_sdnum = 1;       % 3 default max radius of cluster in std devs. highere values less clusters
Do_clustering(channels, 'parallel', false, 'par', params );
fprintf('Automated clustering took %.2f Min.\n', toc/60);
fprintf('Done \n****\n')
